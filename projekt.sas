/* 
RADOSŁAW PIOTROWSKI
PROJEKT SAS

KOD OPISANY SZCZEGÓŁOWO W README.ZIP
*/
FILENAME ImportDB 'FILE PATH';

PROC IMPORT DATAFILE=ImportDB DBMS=CSV OUT=PokemonsDB(rename=_=Index) replace;
	GETNAMES=YES;
	GUESSINGROWS=32767;
RUN;

PROC CONTENTS DATA=PokemonsDB;
RUN;

PROC PRINT DATA=PokemonsDB(obs=10);
RUN;

PROC PRINT DATA=PokemonsDB;
	WHERE Legendary='True';
RUN;

DATA kolumna_wyliczona;
	SET PokemonsDB;
	STYLE=ATTACK - DEFENSE;
RUN;

DATA Trenerzy;
	LENGTH Trener $20. Name $20.;
	INPUT Trener Name;
	DATALINES;
Maria Bulbasaur
John Squirtle
Emily Metapod
;
RUN;

DATA Info;
	SET PokemonsDB(keep=Name Total Type_1);
RUN;

PROC SORT DATA=Trenerzy;
	BY Name;
RUN;

PROC SORT data=Info;
	BY Name;
RUN;

DATA Mergujemy;
	MERGE Info(in=pierwszy) Trenerzy(in=drugi);
	BY Name;

	If Drugi;
RUN;

DATA Warunkowe;
	SET Mergujemy;
	LENGTH Swimmer$30.;

	IF Type_1='Water' THEN
		Swimmer='Istny pirat';
	ELSE
		Swimmer='Szczur lądowy';
RUN;

DATA Petla;
	SET Mergujemy(KEEP=Name Total);

	DO Miesiac=1 TO 12;
		Total=ROUND(Total + Total*0.01, 0.1);
		OUTPUT;
	END;
RUN;

DATA Goto;
	SET PokemonsDB;
	KEEP Name TotalSum;

	IF Generation<2 THEN
		GOTO Etykieta1;
	ELSE
		STOP;
Etykieta1:
	TotalSum+Total;
RUN;

PROC FORMAT;
	VALUE  $Legenda 'True'='Legendarny' 'False'='Zwykły';
RUN;

PROC FORMAT;
	VALUE Moc LOW- 300="Low" 300<-450="Medium" 450<-500="High" 
		500 - HIGH="Super High";
RUN;

PROC SORT DATA=PokemonsDB OUT=PDBS;
	BY Type_1 Total;
RUN;

ODS GRAPHICS ON;

PROC FREQ DATA=PDBS ORDER=freq;
	BY Type_1;
	FORMAT Legendary $Legenda. Total Moc.;
	TABLES Legendary*Total /PLOTS=FREQPLOT(TYPE=DOT SCALE=PERCENT);
RUN;

ODS GRAPHICS OFF;

PROC SGPLOT DATA=PokemonsDB;
	SERIES x=Total y=Attack;
	SERIES x=Total y=Defense;
	SERIES x=Total y=HP;
RUN;

DATA Ciekawy_wykres;
	Set PokemonsDB;
	WHERE Total > 660;
RUN;

PROC SORT DATA=Ciekawy_wykres;
	BY Total Defense;
RUN;

PROC SGPLOT DATA=Ciekawy_wykres;
	SERIES x=Total y=Attack;
	SERIES x=Total y=Defense;
RUN;

PROC MEANS DATA=PokemonsDB MAX MIN MEAN VAR STD RANGE MAXDEC=2;
	CLASS Generation Legendary;
	VAR Total Attack Defense HP;
	FORMAT Legendary $legenda.;
RUN;

PROC MEANS DATA=PokemonsDB MAX MIN MEAN MAXDEC=2;
	CLASS Type_1;
	VAR Total Attack Defense HP;
	FORMAT Legendary $legenda.;
	WHERE Legendary='True';
RUN;

PROC SURVEYSELECT DATA=PokemonsDB METHOD=SRS N=4 OUT=Wybrancy;
RUN;

DATA Tablice(DROP=i);
	SET Wybrancy;
	DROP Index Type_1 Type_2 Total Generation Legendary Sp__Atk Sp__Def HP Attack 
		Defense Speed;
	ARRAY Skill{3} Attack Defense Speed;
	ARRAY Ocena{3} $10 Atak Obrona Szybkosc;
	ARRAY Srednia{3}_TEMPORARY_ (79.00, 73.84, 68.28);

	DO i=1 to 3;

		IF ROUND(Skill{i}, 0.01) > Srednia{i} THEN
			Ocena{i}='> średnia';
		ELSE
			Ocena{i}='< średnia';
	END;
RUN;

%macro PickPokemons(pierwszy, drugi, Base);
	DATA &Base;
		SET PokemonsDB;
		ID=_N_;
	RUN;

	DATA &Base;
		SET &Base;
		WHERE ID=&pierwszy OR ID=&drugi;
		KEEP Name Attack Defense HP;
	run;

	DATA &Base;
		SET &Base;
		A1=Lag(Attack);
		D1=Lag(Defense);
		HP1=Lag(HP);

		IF _N_=1 THEN
			DO;
				A1=Attack;
				D1=Defense;
				HP1=HP;
			END;

		IF _N_=2 THEN
			DO;
				A2=Attack;
				D2=Defense;
				HP2=HP;
			END;
	RUN;

	PROC SORT DATA=&Base;
		BY DESCENDING A2 D2 HP2;
	run;

	DATA &Base(DROP=FilledA FilledD FilledHP Attack Defense HP);
		SET &Base;
		RETAIN filledA;
		RETAIN filledD;
		RETAIN filledHP;

		IF NOT MISSING(A2) THEN
			filledA=A2;

		IF NOT MISSING(D2) THEN
			filledD=D2;

		IF NOT MISSING(HP2) THEN
			filledHP=HP2;
		A2=filledA;
		D2=filledD;
		HP2=filledHP;
	RUN;

	DATA &Base(DROP=Name);
		SET &Base(obs=1);
	RUN;

	DATA &Base(KEEP=Runda HP1 HP2);
		SET &Base;
		Runda=1;

		IF (HP1+A1+D1 > HP2+A2+D2) THEN
			DO;

				DO WHILE(HP1>0 AND HP2>0);

					IF MOD(Runda, 2)=0 AND Runda<>1 THEN
						DO;
							HP2=ROUND(HP2 - (5*A1/(D2-Runda)+ 2), 1);
						END;
					ELSE
						DO;
							HP1=ROUND(HP1 - (5*A2/(D1-Runda)+ 2), 1);
						END;
					OUTPUT;
					Runda=Runda + 1;
				END;
			END;
		ELSE
			DO;

				DO WHILE(HP1>0 AND HP2>0);

					IF MOD(Runda, 2)=0 AND Runda<>1 THEN
						DO;
							HP1=ROUND(HP1 - (5*A2/(D1-Runda)+ 2), 1);
						END;
					ELSE
						DO;
							HP2=ROUND(HP2 - (5*A1/(D2-Runda)+ 2), 1);
						END;
					OUTPUT;
					Runda=Runda + 1;
				END;
			END;
	RUN;

%mend PickPokemons;

%PickPokemons(1, 2, Fight);

PROC SGPLOT DATA=PokemonsDB(WHERE=(Type_1='Water' AND Generation=4)) 
		NOAUTOLEGEND;
	BUBBLE X=Attack Y=SP__Atk SIZE=Total / GROUP=Name DATALABEL=Name 
		TRANSPARENCY=0.4 DATALABELATTRS=(SIZE=9 WEIGHT=bold);
	INSET "Total" / POSITION=BOTTOMRIGHT TEXTATTRS=(SIZE=11);
	YAXIS GRID;
	XAXIS GRID;
RUN;

PROC UNIVARIATE DATA=PokemonsDB NOPRINT;
	HISTOGRAM Generation / NORMAL BARLABEL=percent MIDPOINTS=1 to 6 by 1;
RUN;

proc template;
	define statgraph SASStudio.Pie;
		begingraph;
		layout region;
		piechart category=Generation / stat=pct;
		endlayout;
		endgraph;
	end;
run;


proc sgrender template=SASStudio.Pie data=WORK.POKEMONSDB;
run;

proc template;
	define statgraph SASStudio.Pie;
		begingraph;
		layout region;
		piechart category=Generation / stat=pct;
		endlayout;
		endgraph;
	end;
run;

proc sgrender template=SASStudio.Pie data=WORK.POKEMONSDB;
run;

proc sgplot data=WORK.POKEMONSDB;
	vbox Total / category=Type_1;
	yaxis grid;
run;

